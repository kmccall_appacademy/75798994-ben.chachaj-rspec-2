#require "time"

def measure(number=1, &prc)
  start_time = Time.now
  number.times {prc.call}
  (Time.now - start_time) / number
end
